import {Component, Input, Output, OnInit, EventEmitter} from '@angular/core';
import {PostsService} from '../services/posts.service';

@Component({
  selector: 'app-post1',
  templateUrl: './post1.component.html',
  styleUrls: ['./post1.component.scss']
})
export class Post1Component implements OnInit {
  @Input() postName: string;
  @Input() postStatus: string;
  @Input() postContent: string;
  @Input() loveIts: number;
  @Input() index: number;
  @Input() id: number;
  @Output() colorChange = new EventEmitter();


  lastUpdate = new Promise((resolve, reject) => {
    const date = new Date();
    setTimeout(
      () => {
        resolve(date);
      }, 2000
    );
  });


  constructor(private postsService: PostsService) {}

  ngOnInit() {
  }

  onDelete(id) {
    this.postsService.delete(id);
  }
  love(index) {
    this.postsService.lovemethod(index);
  }
  nolove(index) {
    this.postsService.nolovemethod(index);
  }
}
