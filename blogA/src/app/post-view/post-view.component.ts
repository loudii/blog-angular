import { Component, OnInit } from '@angular/core';
import {PostsService} from '../services/posts.service';

@Component({
  selector: 'app-post-view',
  templateUrl: './post-view.component.html',
  styleUrls: ['./post-view.component.scss']
})
export class PostViewComponent implements OnInit {

  post: any[];

  constructor(private postsService: PostsService) { }

  ngOnInit() {
    this.post = this.postsService.post;
  }

}
