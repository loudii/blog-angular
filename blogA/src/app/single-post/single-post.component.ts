import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {PostsService} from '../services/posts.service';
import {ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.scss']
})
export class SinglePostComponent implements OnInit {

  name: string = 'posts';
  content: string = 'content';
  loveIts: number = 0;
  @Output() colorChange = new EventEmitter();

  constructor(private postsService: PostsService,  private route: ActivatedRoute) { }

  lastUpdate = new Promise((resolve, reject) => {
    const date = new Date();
    setTimeout(
      () => {
        resolve(date);
      }, 2000
    );
  });

  ngOnInit() {
    const id = this.route.snapshot.params['id'];
    this.name = this.postsService.getPostsById(+id).name;
    this.content = this.postsService.getPostsById(+id).content;
  }

  love() {
    this.loveIts++;
  }

  nolove() {
    this.loveIts--;
  }

}
