import {EventEmitter, Injectable, Output} from '@angular/core';


@Injectable()
export class PostsService {

  loveIts: number = 0;


  post: any[] = [
    {
      id: 1,
      name: 'Mon Post 1',
      content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A adipisci aliquid atque commodi dolorem eos facilis hic, itaque laudantium libero maxime nam nemo nobis nulla reiciendis tempore velit veritatis voluptatum?',
      loveIts: 0,
      index: 1
    },
    {
      id: 2,
      name: 'Mon Post 2',
      content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A adipisci aliquid atque commodi dolorem eos facilis hic, itaque laudantium libero maxime nam nemo nobis nulla reiciendis tempore velit veritatis voluptatum?',
      loveIts: 0,
      index: 2
    },
    {
      id: 3,
      name: 'Mon Post 3',
      content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A adipisci aliquid atque commodi dolorem eos facilis hic, itaque laudantium libero maxime nam nemo nobis nulla reiciendis tempore velit veritatis voluptatum?',
      loveIts: 0,
      index: 3
    }
  ];
  constructor() { }

  getPostsById(id: number) {
    const post = this.post.find(
      (s) => {
        return s.id === id;
      }
    );
    return post;
  }
  addPost(name: string, content: string, loveIts: number) {
    const postObject = {
      id: 0,
      name: '',
      date: new Date(),
      content: '',
      loveIts: 0
    };
    postObject.name = name;
    postObject.content = content;
    postObject.loveIts = loveIts;
    postObject.id = this.post[(this.post.length - 1)].id + 1;
    this.post.push(postObject);
  }
  delete(id) {
    const index = this.post.map(function(x) { return x.id; }).indexOf(id);
    this.post.splice(index, 1);
  }
  lovemethod(index) {
    this.post[index].loveIts++;
  }

  nolovemethod(index) {
    this.post[index].loveIts--;
  }
}
