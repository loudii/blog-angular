import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { Post1Component } from './post1/post1.component';
import { FormsModule } from '@angular/forms';
import { PostsService } from './services/posts.service';
import { NewComponent } from './new/new.component';
import { PostViewComponent } from './post-view/post-view.component';
import {RouterModule, Routes} from '@angular/router';
import { SinglePostComponent } from './single-post/single-post.component';

const appRoutes: Routes = [
  { path: 'posts', component: PostViewComponent },
  { path: '', component: PostViewComponent },
  { path: 'new', component: NewComponent },
  { path: 'posts/:id', component: SinglePostComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    Post1Component,
    NewComponent,
    PostViewComponent,
    SinglePostComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    PostsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
